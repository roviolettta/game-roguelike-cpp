#ifndef MAIN_PLAYER_H
#define MAIN_PLAYER_H

#include "Image.h"


struct Point
{
  int x;
  int y;
};

enum class MovementDir
{
  UP,
  DOWN,
  LEFT,
  RIGHT,
  ATTACK
};

struct Player
{
  explicit Player(Point pos = {.x = 10, .y = 10}) :
                 coords(pos), old_coords(coords) {};

  bool Moved() const;

  bool CheckWall(MovementDir dir, char Field[40][40]) const;
  void CheckSpace(Image &screen, char Field[40][40]);

  int  ProcessInput(MovementDir dir, char Field[40][40]);
  void ProcessInput_act(MovementDir dir, Image &screen, char Field[40][40]);
  
  Pixel mix(Pixel bufPixel, Pixel picPixel);

  void Draw(Image &screen,char Field[40][40]);
  void DrawBackground(Image &screen,char Field[40][40],Image &hero);
  void DrawFloor(Image &screen, char Field[40][40],Image &hero);
  void DrawNewState(Image &screen,char Field[40][40]);
  void DrawHealth(Image &screenBuffer, char Field[40][40]);

  void CollectBonuses(Image &screen,char Field[40][40]);

  MovementDir course = MovementDir::RIGHT;
  MovementDir old_course = MovementDir::RIGHT;

  void DrawEnemy(Image &screen,char Field[40][40]);

  Point coords {.x = 10, .y = 10};
  Point old_coords {.x = 10, .y = 10};
  int health = 2;

private:
  
  Pixel color {.r = 255, .g = 255, .b = 0, .a = 255};
  int move_speed = 4;
};

#endif //MAIN_PLAYER_H
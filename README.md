## Roguelike 2D game in C++

Watch the video of gameplay

https://drive.google.com/file/d/1H4MEnfaYRA3ZW7VXZdJiNKzptmPCtE-H/view?usp=sharing

# Launch

Program uses OpenGL 4.6 library

```
cmake .
make
./bin/main
```

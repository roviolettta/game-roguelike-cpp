#include "Player.h"
#include <stdio.h>
#include <iostream>
#include "common.h"

using namespace std;

int win = 0;

bool Player::Moved() const
{
  if(coords.x == old_coords.x && coords.y == old_coords.y)
    return false;
  else
    return true;
}

bool Player::CheckWall(MovementDir dir,char Field[40][40]) const
{
  int current_x, current_y, num_pix_x, num_pix_y;
  
  Image floor("resources/1.png");

  switch(dir)
  {
    case MovementDir::UP:

      current_x=coords.x+move_speed*3;
      current_y=coords.y-move_speed;
      while(current_x<coords.x+tileSize*2-move_speed*3)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ')
        {
          win = -1;
          return true;
        }

        num_pix_x=current_x%tileSize;
        current_x+=tileSize-num_pix_x;
      }

      current_y=coords.y+move_speed+tileSize*2-1;
      current_x=coords.x;
      while(current_x<coords.x+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='#'|| Field[(current_y-indent)/tileSize][current_x/tileSize]=='%') return false;

        num_pix_x=current_x%tileSize;
        current_x+=tileSize-num_pix_x;
      }

      current_x=coords.x;
      win = 1;
      while(current_x<coords.x+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]!='x')
        {
          win = 0;
          break;
        }

        num_pix_x=current_x%tileSize;
        current_x+=tileSize-num_pix_x;
      }

      break;
    case MovementDir::DOWN:

      current_x=coords.x+move_speed*3;
      current_y=coords.y+move_speed;
      while(current_x<coords.x+tileSize*2-move_speed*3)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ')
        {
          win = -1;
          return true;
        }

        num_pix_x=current_x%tileSize;
        current_x+=tileSize-num_pix_x;
      }

      current_y=coords.y-move_speed;
      current_x=coords.x;
      while(current_x<coords.x+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='#'|| Field[(current_y-indent)/tileSize][current_x/tileSize]=='%') return false;

        num_pix_x=current_x%tileSize;
        current_x+=tileSize-num_pix_x;
      }

      current_x=coords.x;
      win = 1;
      while(current_x<coords.x+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]!='x')
        {
          win = 0;
          break;
        }

        num_pix_x=current_x%tileSize;
        current_x+=tileSize-num_pix_x;
      }
      break;
    case MovementDir::LEFT:
      current_x=coords.x+move_speed*2;
      current_y=coords.y;
      while(current_y<coords.y+tileSize)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ')
        {
          win = -1;
          return true;
        }

        num_pix_y=current_y%tileSize;
        current_y+=tileSize-num_pix_y;
      }

      current_x=coords.x-move_speed;
      current_y=coords.y;
      while(current_y<coords.y+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='#'|| Field[(current_y-indent)/tileSize][current_x/tileSize]=='%') return false;

        num_pix_y=(current_y-indent)%tileSize;
        current_y+=tileSize-num_pix_y;
      }

      current_x=coords.x+tileSize;
      current_y=coords.y;
      win = 1;
      while(current_y<coords.y+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]!='x')
        {
          win = 0;
          break;
        }

        num_pix_y=current_y%tileSize;
        current_y+=tileSize-num_pix_y;
      }
      break;
    case MovementDir::RIGHT:
      current_x=coords.x+tileSize*2-move_speed*2;
      current_y=coords.y;
      while(current_y<coords.y+tileSize)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ')
        {
          win = -1;
          return true;
        }

        num_pix_y=current_y%tileSize;
        current_y+=tileSize-num_pix_y;
      }
      current_x=coords.x+move_speed+tileSize*2-1;
      current_y=coords.y;
      while(current_y<coords.y+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='#' || Field[(current_y-indent)/tileSize][current_x/tileSize]=='%') return false;

        num_pix_y=(current_y-indent)%tileSize;
        current_y+=tileSize-num_pix_y;
      }
      
      current_y=coords.y;
      win = 1;
      while(current_y<coords.y+tileSize*2)
      {
        if(Field[(current_y-indent)/tileSize][current_x/tileSize]!='x')
        {
          win = 0;
          break;
        }

        num_pix_y=current_y%tileSize;
        current_y+=tileSize-num_pix_y;
      }
      break;
    default:
      break;
  }

  return true;
}

void Player::CheckSpace(Image &screen,char Field[40][40])
{
  int current_x, current_y, num_pix_x, num_pix_y;
  Image floor("resources/1.png");

  if(course==MovementDir::RIGHT || course==MovementDir::LEFT){
    if(course==MovementDir::RIGHT) current_x=coords.x+tileSize*2; 
    else current_x=coords.x-1;

    current_y=coords.y;
    while(current_y<coords.y+tileSize*2){

        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='%'){
          for(int y = 0, i = current_y-num_pix_y; y < tileSize; y++,i++)
          {
            for(int x = 0, j = current_x-num_pix_x; x < tileSize; x++,j++)
            {
              screen.PutPixel(j, i, floor.GetPixel(x, tileSize-y-1));
            }
          }

          Field[(current_y-indent)/tileSize][current_x/tileSize]='.';
        }
        current_y+=tileSize-num_pix_y;
      }
  }

  else if (course==MovementDir::UP || course==MovementDir::DOWN)
  {
    if(course==MovementDir::UP) current_y=coords.y+tileSize*2;
    else current_y=coords.y-1;

    current_x=coords.x;

    while(current_x<coords.x+tileSize*2){

        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='%'){
          for(int y = 0, i = current_y-num_pix_y; y < tileSize; y++,i++)
          {
            for(int x = 0, j = current_x-num_pix_x; x < tileSize; x++,j++)
            {
              screen.PutPixel(j, i, floor.GetPixel(x, tileSize-y-1));
            }
          }

          Field[(current_y-indent)/tileSize][current_x/tileSize]='.';
        }
        current_x+=tileSize-num_pix_x;
      }
  }
}

int Player::ProcessInput(MovementDir dir, char Field[40][40])
{
  int move_dist = move_speed * 1;
  switch(dir)
  {
    case MovementDir::UP:
      if(CheckWall(MovementDir::UP,Field)){
        old_coords.y = coords.y;
        coords.y += move_dist;
      }  

        old_course = course;
        course = MovementDir::UP; 
      break;
    case MovementDir::DOWN:
      if(CheckWall(MovementDir::DOWN,Field)){
        old_coords.y = coords.y;
        coords.y -= move_dist;
      }

      old_course = course;
      course = MovementDir::DOWN;
      break;
    case MovementDir::LEFT:
      if(CheckWall(MovementDir::LEFT,Field)){
        old_coords.x = coords.x;
        coords.x -= move_dist;
      }

      old_course = course;
      course = MovementDir::LEFT;
      break;
    case MovementDir::RIGHT:
      if(CheckWall(MovementDir::RIGHT,Field)){
        old_coords.x = coords.x;
        coords.x += move_dist;
      }

      old_course = course;
      course = MovementDir::RIGHT;
      break;
    default:
      break;
  }

  return win;
}

void Player::ProcessInput_act(MovementDir dir, Image &screen, char Field[40][40])
{
  int move_dist = move_speed * 1;
  switch(dir)
  {
    case MovementDir::ATTACK:
      CheckSpace(screen, Field);
      break;
    default:
      break;
  }
}

void Player::DrawFloor(Image &screen, char Field[40][40],Image &hero)
{
  Image floor("resources/1.png");
  Image empty("resources/empty.png");

  int num_pix_x, num_pix_y; 
  int current_x, current_y;
  int dx, dy; 

  current_x = old_coords.x;
  current_y = old_coords.y;

  if(old_coords.x==coords.x && old_coords.y==coords.y)
  {
    dx = coords.x+tileSize*2;
    dy = coords.y+tileSize*2;

    while(current_x < dx){
      while(current_y < dy)
      {      
        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='.'|| Field[(current_y-indent)/tileSize][current_x/tileSize]=='@' 
        || Field[(current_y-indent)/tileSize][current_x/tileSize]=='*'){
          for(num_pix_y,current_y; num_pix_y < tileSize && current_y < dy; num_pix_y++,current_y++)
          {
            for(int i = num_pix_x, j = current_x; i < tileSize && j < dx; i++,j++)
            {
              if(hero.GetPixel(j-old_coords.x,tileSize*2-(current_y-old_coords.y)-1).a !=0)
                screen.PutPixel(j, current_y, floor.GetPixel(i, tileSize-num_pix_y-1));
            }
          }
        }
        else{
          current_y+=tileSize-num_pix_y;
        }
      }

      current_x+=tileSize-num_pix_x;
      current_y = old_coords.y;
    }
  }

  else if(old_coords.y==coords.y)
  {
    if(old_coords.x<coords.x) dx = coords.x+tileSize*2;
    else dx = old_coords.x+tileSize*2;
    while(current_x < dx){
      while(current_y < coords.y+tileSize*2)
      {      
        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='.'|| Field[(current_y-indent)/tileSize][current_x/tileSize]=='@'
        || Field[(current_y-indent)/tileSize][current_x/tileSize]=='*'){
          for(num_pix_y,current_y; num_pix_y < tileSize && current_y < coords.y+tileSize*2; num_pix_y++,current_y++)
          {
            for(int i = num_pix_x, j = current_x; i < tileSize && j < dx; i++,j++)
            {
              if(hero.GetPixel(j-old_coords.x,tileSize*2-(current_y-old_coords.y)-1).a !=0)
                screen.PutPixel(j, current_y, floor.GetPixel(i, tileSize-num_pix_y-1));
            }
          }
        }
        else{
          current_y+=tileSize-num_pix_y;
        }
      }

      current_x+=tileSize-num_pix_x;
      current_y = old_coords.y;
    }
  }
  else if(old_coords.x==coords.x)
  {
    if(old_coords.y<coords.y) dy = coords.y+tileSize*2;
    else dy = old_coords.y+tileSize*2;
    while(current_y < dy){
      while(current_x < coords.x+tileSize*2)
      {
        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]=='.' || Field[(current_y-indent)/tileSize][current_x/tileSize]=='@'
        || Field[(current_y-indent)/tileSize][current_x/tileSize]=='*'){
          for(num_pix_x, current_x; num_pix_x < tileSize && current_x < coords.x+tileSize*2; num_pix_x++,current_x++)
          {
            for(int i = current_y, j = num_pix_y; i < dy, j<tileSize; j++, i++)
            {
              if(hero.GetPixel(current_x-old_coords.x,tileSize*2-(i-old_coords.y)-1).a !=0)
                screen.PutPixel(current_x, i, floor.GetPixel(num_pix_x, tileSize-j-1));
            }
          }
        }

        else{
          current_x+=tileSize-num_pix_x;
        } 
      }

      current_y+=tileSize-num_pix_y;
      current_x = old_coords.x;
    }
  }
}

void Player::DrawBackground(Image &screen,char Field[40][40],Image &hero)
{
  int num_pix_x, num_pix_y; 
  int current_x, current_y;
  int dx, dy; 
  int remember;

  Image floor("resources/1.png");
  Image wall("resources/2.png");
  Image empty("resources/empty.png");

  DrawFloor(screen, Field, hero);

  current_x = old_coords.x;
  current_y = old_coords.y;

  if(old_coords.y==coords.y)
  { 
    if(old_coords.x<coords.x) dx = coords.x+tileSize*2;
    else dx = old_coords.x+tileSize*2;

    while(current_x < dx){
      while(current_y < coords.y+tileSize*2)
      {
        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;
        remember=current_y;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ')
        {
          if(Field[(current_y-indent)/tileSize+1][current_x/tileSize]!=' ') num_pix_y+=tileSize;
          if(Field[(current_y-indent)/tileSize][current_x/tileSize+1]!=' ') num_pix_x+=tileSize;
          for(num_pix_y,current_y; num_pix_y < tileSize*2 && current_y < coords.y+tileSize*2; num_pix_y++,current_y++)
          {
            for(int i = num_pix_x, j = current_x; i < tileSize*2 && j < dx; i++,j++)
            {
              screen.PutPixel(j, current_y, empty.GetPixel(i, tileSize*2-num_pix_y-1));
            }
          }

          break;
        }

        else
        {
          current_y=current_y+tileSize-num_pix_y;
        }
      }

      current_y = remember;
      if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ') break;

      current_x+=tileSize-num_pix_x;
      current_y = old_coords.y;
    }
  }

  else{
    if(old_coords.y<coords.y) dy = coords.y+tileSize*2;
    else dy = old_coords.y+tileSize*2;
    
    while(current_y < dy){
      while(current_x < coords.x+tileSize*2)
      {
        num_pix_x=current_x%tileSize;
        num_pix_y=(current_y-indent)%tileSize;
        remember=current_x;

        if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ')
        {
          if(Field[(current_y-indent)/tileSize+1][current_x/tileSize]!=' ') num_pix_y+=tileSize;
          if(Field[(current_y-indent)/tileSize][current_x/tileSize+1]!=' ') num_pix_x+=tileSize;
          for(num_pix_x,current_x; num_pix_x < tileSize*2 && current_x < coords.x+tileSize*2; num_pix_x++,current_x++)
          {
            for(int i = num_pix_y, j = current_y; i < tileSize*2 && j < dy; i++,j++)
            {
              screen.PutPixel(current_x, j, empty.GetPixel(num_pix_x, tileSize*2-i-1));
            }
          }

          break;
        }

        else
        {
          current_x=current_x+tileSize-num_pix_x;
        }
      }

      current_x = remember;
      if(Field[(current_y-indent)/tileSize][current_x/tileSize]==' ') break;

      current_y+=tileSize-num_pix_y;
      current_x = old_coords.x;
    }
  }
}

void Player :: DrawHealth(Image &screenBuffer, char Field[40][40])
{
  Image heart("resources/heart.png");
  Image no_heart("resources/no_heart.png");

  for(int x=tileSize, i=1; x < heart.Width()*3+tileSize, i<=3; x+=heart.Width(), i++){
    if(i<=health){
    for(int y1=0; y1 < heart.Height(); y1++)
      for(int x1=x; x1 < x + heart.Height(); x1++)
        screenBuffer.PutPixel(x1,y1+tileSize,heart.GetPixel(x1-x, heart.Width()-y1-1));
    }

    else{
    for(int y1=0; y1 < heart.Height(); y1++)
      for(int x1=x; x1 < x + heart.Height(); x1++)
        screenBuffer.PutPixel(x1,y1+tileSize,no_heart.GetPixel(x1-x, no_heart.Width()-y1-1));
    }
  }
}

void Player :: CollectBonuses(Image &screen,char Field[40][40])
{
  Image floor("resources/1.png");
  Image antidote("resources/antidote.png");

  int num_pix_x, num_pix_y; 
  int current_x, current_y;
  int x, y; 

  current_x = coords.x;
  current_y = coords.y;

  while(current_x < coords.x+tileSize*2){
    while(current_y < coords.y+tileSize*2)
    {      
      num_pix_x=current_x%tileSize;
      num_pix_y=(current_y-indent)%tileSize;

      x=current_x/tileSize;
      y=(current_y-indent)/tileSize;

      if(Field[y][x]=='*') {
        for(int j=0, img_y = y*tileSize+indent; j<tileSize; j++, img_y++)
        {
          for(int i=0, img_x = x*tileSize; i<tileSize; i++, img_x++)
          {
            if(antidote.GetPixel(i,tileSize-j-1).a !=0)
              screen.PutPixel(img_x, img_y, floor.GetPixel(i, tileSize-j-1));
          }
        }

        break;
      }
      else{
        current_y+=tileSize-num_pix_y;
      }
    }

    if(Field[y][x]=='*') 
    {
      Field[y][x]='.';
      if(health<3) health++;
      DrawHealth(screen, Field);
      break;
    }

    current_x+=tileSize-num_pix_x;
    current_y = coords.y;
  }
}

Pixel Player :: mix(Pixel bufPixel, Pixel picPixel)
{
  picPixel.r = picPixel.a / 255.0 * (picPixel.r - bufPixel.r) + bufPixel.r;
  picPixel.g = picPixel.a / 255.0 * (picPixel.g - bufPixel.g) + bufPixel.g; 
  picPixel.b = picPixel.a / 255.0 * (picPixel.b - bufPixel.b) + bufPixel.b;
  picPixel.a = 255;

  return picPixel;
}

void Player::Draw(Image &screen,char Field[40][40])
{
  Image hero("resources/r.png");
  Image hero1("resources/d.png");
  Image hero2("resources/u.png"); 
  Image hero3("resources/l.png");
  Image floor("resources/1.png");

  if(Moved()) 
  {
    CollectBonuses(screen, Field);
    switch(old_course)
    {
    case MovementDir::UP:
      DrawBackground(screen,Field,hero2);
      break;
    case MovementDir::DOWN:
      DrawBackground(screen,Field,hero1);
      break;
    case MovementDir::LEFT:
      DrawBackground(screen,Field,hero3);
      break;
    case MovementDir::RIGHT:
      DrawBackground(screen,Field,hero);
      break;
    default:
      break;
    } 

    old_coords = coords;
  }
  else{
    switch(old_course)
    {
    case MovementDir::UP:
      DrawFloor(screen, Field, hero2);
      break;
    case MovementDir::DOWN:
      DrawFloor(screen, Field, hero1);
      break;
    case MovementDir::LEFT:
      DrawFloor(screen, Field, hero3);
      break;
    case MovementDir::RIGHT:
      DrawFloor(screen, Field, hero);
      break;
    default:
      break;
    } 
  }
  
  old_course = course;

  
  switch(course)
  {
    case MovementDir::UP:
      for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {
        for(int x = coords.x; x < coords.x + tileSize*2; ++x)
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero2.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    case MovementDir::DOWN:
      for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {   
        for(int x = coords.x; x < coords.x + tileSize*2; ++x)
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero1.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    case MovementDir::LEFT:
     for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {   
        for(int x = coords.x; x < coords.x + tileSize*2; ++x) 
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero3.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    case MovementDir::RIGHT:
      for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {   
        for(int x = coords.x; x < coords.x + tileSize*2; ++x)
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    default:
      break;
  }
}

void Player::DrawNewState(Image &screen,char Field[40][40])
{
  Image hero("resources/r.png");
  Image hero1("resources/d.png");
  Image hero2("resources/u.png"); 
  Image hero3("resources/l.png");
  Image floor("resources/1.png");
  
  switch(course)
  {
    case MovementDir::UP:
      for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {
        for(int x = coords.x; x < coords.x + tileSize*2; ++x)
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero2.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    case MovementDir::DOWN:
      for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {   
        for(int x = coords.x; x < coords.x + tileSize*2; ++x)
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero1.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    case MovementDir::LEFT:
     for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {   
        for(int x = coords.x; x < coords.x + tileSize*2; ++x) 
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero3.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    case MovementDir::RIGHT:
      for(int y = coords.y; y < coords.y + tileSize*2; ++y)
      {   
        for(int x = coords.x; x < coords.x + tileSize*2; ++x)
          screen.PutPixel(x, y, mix(screen.GetPixel(x,y),hero.GetPixel(x-coords.x, coords.y+tileSize*2-y-1)));
      }
      break;
    default:
      break;
  }
}
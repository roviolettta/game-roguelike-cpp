#include "common.h"
#include "Image.h"
#include "Player.h"
#include <fstream> 
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <math.h>

using namespace std;

#define GLFW_DLL
#include <GLFW/glfw3.h>

constexpr GLsizei WINDOW_WIDTH = 700, WINDOW_HEIGHT = 700;

struct InputState
{
  bool keys[1024]{}; //массив состояний кнопок - нажата/не нажата
  GLfloat lastX = 400, lastY = 300; //исходное положение мыши
  bool firstMouse = true;
  bool captureMouse         = true;  // Мышка захвачена нашим приложением или нет?
  bool capturedMouseJustNow = false;
} static Input;


GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;


void OnKeyboardPressed(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	switch (key)
	{
	case GLFW_KEY_ESCAPE:
		if (action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GL_TRUE);
		break;
  case GLFW_KEY_1:
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    break;
  case GLFW_KEY_2:
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    break;
	default:
		if (action == GLFW_PRESS)
      Input.keys[key] = true;
		else if (action == GLFW_RELEASE)
      Input.keys[key] = false;
	}
}

int processPlayerMovement(Player &player,Image &screen, char Field[40][40])
{
  if (Input.keys[GLFW_KEY_W]){
    return player.ProcessInput(MovementDir::UP,Field);
  }
  else if (Input.keys[GLFW_KEY_S]){
    return player.ProcessInput(MovementDir::DOWN,Field);
  }
  if (Input.keys[GLFW_KEY_A]){
    return player.ProcessInput(MovementDir::LEFT,Field);
  }
  else if (Input.keys[GLFW_KEY_D]){
    return player.ProcessInput(MovementDir::RIGHT,Field); 
  }
  else if (Input.keys[GLFW_KEY_Q]){
    player.ProcessInput_act(MovementDir::ATTACK,screen,Field); 
  }

  return 0;
}

void OnMouseButtonClicked(GLFWwindow* window, int button, int action, int mods)
{
  if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
    Input.captureMouse = !Input.captureMouse;

  if (Input.captureMouse)
  {
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    Input.capturedMouseJustNow = true;
  }
  else
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

}

void OnMouseMove(GLFWwindow* window, double xpos, double ypos)
{
  if (Input.firstMouse)
  {
    Input.lastX = float(xpos);
    Input.lastY = float(ypos);
    Input.firstMouse = false;
  }

  GLfloat xoffset = float(xpos) - Input.lastX;
  GLfloat yoffset = Input.lastY - float(ypos);

  Input.lastX = float(xpos);
  Input.lastY = float(ypos);
}


void OnMouseScroll(GLFWwindow* window, double xoffset, double yoffset)
{
  // ...
}


int initGL()
{
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		return -1;
	}

	std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "Version: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

  std::cout << "Controls: "<< std::endl;
  std::cout << "press right mouse button to capture/release mouse cursor  "<< std::endl;
  std::cout << "W, A, S, D - movement  "<< std::endl;
  std::cout << "Q - tear down the walls  "<< std::endl;
  std::cout << "press ESC to exit" << std::endl;

	return 0;
}

void DrawWindow(char *name, Image &screenBuffer, char Field[40][40], Player &player)
{
  ifstream field(name);

  for(int i=0;i<40;i++){
    for(int j=0;j<40;j++){
      Field[i][j] = field.get();
      if(Field[i][j]=='\n') Field[i][j] = field.get();
    }
  }

  Image floor("resources/1.png");
	Image wall("resources/2.png");
  Image exit("resources/exit.png");
  Image crash("resources/3.png");
  Image empty("resources/empty.png");
  Image antidote("resources/antidote.png");
  
  player.DrawHealth(screenBuffer, Field);

  for(int y=indent,i=0; y < floor.Height()*40+indent; y+=floor.Height(),i++){
    for(int x=0,j=0; x < floor.Width()*40; x+=floor.Width(),j++){
      if(Field[i][j]=='#'){
        for(int y1=y; y1 < y + floor.Height(); y1++)
          for(int x1=x; x1 < x + floor.Height(); x1++)
            screenBuffer.PutPixel(x1,y1,wall.GetPixel(x1-x, y+wall.Width()-y1-1));
      }
      else if(Field[i][j]=='.' || Field[i][j]=='@' || Field[i][j]=='*'){
        for(int y1=y; y1 < y + floor.Height(); y1++){
          for(int x1=x; x1 < x + floor.Height(); x1++){
            if(Field[i][j]=='*') screenBuffer.PutPixel(x1,y1, player.mix(floor.GetPixel(x1-x, y+floor.Width()-y1-1),antidote.GetPixel(x1-x, y+antidote.Width()-y1-1)));
            else screenBuffer.PutPixel(x1,y1,floor.GetPixel(x1-x, y+floor.Width()-y1-1));
          }
        }
      }
      else if(Field[i][j]=='x'){
        if(Field[i-1][j]!='x' && Field[i][j-1]!='x'){
          for(int y1=y; y1 < y + exit.Height(); y1++)
            for(int x1=x; x1 < x + exit.Height(); x1++)
              screenBuffer.PutPixel(x1,y1,exit.GetPixel(x1-x, y+exit.Width()-y1-1));
        }
      }
      else if(Field[i][j]==' '){
        if(Field[i-1][j]!=' ' && Field[i][j-1]!=' '){
          for(int y1=y; y1 < y + empty.Height(); y1++)
            for(int x1=x; x1 < x + empty.Height(); x1++)
              screenBuffer.PutPixel(x1,y1,empty.GetPixel(x1-x, y+empty.Width()-y1-1));
        }
      }
      else if(Field[i][j]=='%'){
        for(int y1=y; y1 < y + crash.Height(); y1++)
          for(int x1=x; x1 < x + crash.Height(); x1++)
            screenBuffer.PutPixel(x1,y1,crash.GetPixel(x1-x, y+floor.Width()-y1-1));
      }
    }
  }

  field.close();
}

Pixel Blackout(Pixel bufPixel)
{
  Pixel picPixel; 
  if(bufPixel.r > 0) picPixel.r = bufPixel.r-1;
  else picPixel.r = 0;

  if(bufPixel.g > 0) picPixel.g = bufPixel.g-1;
  else picPixel.g = 0; 

  if(bufPixel.b > 0) picPixel.b = bufPixel.b-1;
  else picPixel.b = 0;

  picPixel.a = bufPixel.a;

  return picPixel;
}

void BlackoutScreen(Image &screenBuffer, GLFWwindow *window){
  Pixel screenPix;

  for(int i=0; i<255; i++){
    for(int y=0; y<WINDOW_HEIGHT; y++){
      for(int x=0; x<WINDOW_WIDTH; x++){
        screenPix=Blackout(screenBuffer.GetPixel(x,y));
        screenBuffer.PutPixel(x, y, screenPix);
      }
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
    glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
    glfwSwapBuffers(window);
  }
}

int main(int argc, char** argv)
{
	if(!glfwInit())
    return -1;

//	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow*  window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "task1 base project", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	
	glfwMakeContextCurrent(window); 

	glfwSetKeyCallback        (window, OnKeyboardPressed);  
	glfwSetCursorPosCallback  (window, OnMouseMove); 
  glfwSetMouseButtonCallback(window, OnMouseButtonClicked);
	glfwSetScrollCallback     (window, OnMouseScroll);

	if(initGL() != 0) 
		return -1;
	
  //Reset any OpenGL errors which could be present for some reason
	GLenum gl_error = glGetError();
	while (gl_error != GL_NO_ERROR)
		gl_error = glGetError();

	//Point starting_pos{.x = WINDOW_WIDTH / 2, .y = WINDOW_HEIGHT / 2};
  Point starting_pos{.x = 16, .y = 16+indent};
	Player player{starting_pos};

  Point starting_pos_enemy{.x = tileSize*34, .y = tileSize*22+indent};
  Player enemy{starting_pos_enemy};
  enemy.course=MovementDir::LEFT;
  enemy.old_course=MovementDir::RIGHT;

	Image screenBuffer(WINDOW_WIDTH, WINDOW_HEIGHT, 4);

  glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);  GL_CHECK_ERRORS;
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f); GL_CHECK_ERRORS;

  int win;
  int level=0;
  char Field[40][40];
  char name[]="resources/field2.txt";
  char name2[]="resources/field.txt";
  Image winn("resources/win.png");
  Image lose("resources/lose.png");
  Image levl("resources/levl.png");

  DrawWindow(name,screenBuffer,Field, player);

  //game loop
	while (!glfwWindowShouldClose(window))
	{
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
    glfwPollEvents();

    win = processPlayerMovement(player, screenBuffer, Field);
    if(win==1){
      level++;
      if(level==2)
      {
        for(int y1=0; y1 < winn.Height(); y1++){
          for(int x1=0; x1 < winn.Width(); x1++){
            screenBuffer.PutPixel(x1+70, y1+70+indent, player.mix(screenBuffer.GetPixel(x1+70,y1+70+indent),winn.GetPixel(x1, winn.Height()-y1-1)));
          }
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
        glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
        glfwSwapBuffers(window);

        sleep(1);
        BlackoutScreen(screenBuffer, window);
        cout<<"вы выиграли"<<endl;
        break;
      }

      for(int y1=0; y1 < levl.Height(); y1++){
        for(int x1=0; x1 < levl.Width(); x1++){
          screenBuffer.PutPixel(x1+70, y1+70+indent, player.mix(screenBuffer.GetPixel(x1+70,y1+70+indent),levl.GetPixel(x1, levl.Height()-y1-1)));
        }
      }

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
      glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
      glfwSwapBuffers(window);

      sleep(1);

      BlackoutScreen(screenBuffer, window);
      DrawWindow(name2,screenBuffer,Field, player);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
      glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
      glfwSwapBuffers(window);

      player.course=MovementDir::DOWN;
   
    } 
    else if(win==-1)
    {
      for(int y1=0; y1 < lose.Height(); y1++){
        for(int x1=0; x1 < lose.Width(); x1++){
          screenBuffer.PutPixel(x1+70, y1+70+indent, player.mix(screenBuffer.GetPixel(x1+70,y1+70+indent),lose.GetPixel(x1, lose.Height()-y1-1)));
        }
      }

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
      glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
      glfwSwapBuffers(window);

      sleep(1);
      BlackoutScreen(screenBuffer, window);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
      glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
      glfwSwapBuffers(window);
      player.health--;

      if(player.health!=0)
      {
        if(level==0)
        {
          player.coords.x=16;
          player.coords.y=indent+16;
          player.old_coords.x=player.coords.x;
          player.old_coords.y=player.coords.y;
          player.course=MovementDir::RIGHT;
          DrawWindow(name,screenBuffer,Field, player);

          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
          glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
          glfwSwapBuffers(window);
        }
        else if(level==1)
        {
          player.coords.x=16*37;
          player.coords.y=indent+16*37;
          player.old_coords.x=player.coords.x;
          player.old_coords.y=player.coords.y;
          player.course=MovementDir::DOWN;
          DrawWindow(name2,screenBuffer,Field, player);

          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
          glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
          glfwSwapBuffers(window);
        }
      }
      else{
        cout<<"игра завершена"<<endl;
        break;
      }
    }

    player.Draw(screenBuffer, Field);
    player.DrawNewState(screenBuffer, Field);

    enemy.DrawEnemy(screenBuffer,Field);

    if(abs(player.coords.x-enemy.coords.x)<tileSize*2 && abs(player.coords.y-enemy.coords.y)<tileSize*2)
    {
      for(int y1=0; y1 < lose.Height(); y1++){
        for(int x1=0; x1 < lose.Width(); x1++){
          screenBuffer.PutPixel(x1+70, y1+70+indent, player.mix(screenBuffer.GetPixel(x1+70,y1+70+indent),lose.GetPixel(x1, lose.Height()-y1-1)));
        }
      }

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
      glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
      glfwSwapBuffers(window);

      sleep(1);
      BlackoutScreen(screenBuffer, window);
      player.health--;

      if(player.health!=0)
      {
        if(level==0)
        {
          player.coords.x=16;
          player.coords.y=indent+16;
          player.old_coords.x=player.coords.x;
          player.old_coords.y=player.coords.y;
          player.course=MovementDir::RIGHT;
          DrawWindow(name,screenBuffer,Field, player);

          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
          glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
          glfwSwapBuffers(window);
        }
        else if(level==1)
        {
          player.coords.x=16*37;
          player.coords.y=indent+16*37;
          player.old_coords.x=player.coords.x;
          player.old_coords.y=player.coords.y;
          player.course=MovementDir::DOWN;
          DrawWindow(name2,screenBuffer,Field, player);

          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
          glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
          glfwSwapBuffers(window);
        }
      }
      else{
        cout<<"игра завершена"<<endl;
        break;
      }
    }

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;
    glDrawPixels (WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer.Data()); GL_CHECK_ERRORS;
		glfwSwapBuffers(window);
	}

	glfwTerminate();
	return 0;
}

#include "Player.h"
#include <stdio.h>
#include <iostream>
#include "common.h"

using namespace std;

void Player::DrawEnemy(Image &screen, char Field[40][40])
{
    Image enemy_l("resources/enemy_left.png");
    Image enemy_r("resources/enemy.png");

    int move_dist = move_speed / 2;

    if (course == MovementDir::RIGHT)
    {
        old_coords.x = coords.x;
        coords.x += move_dist;
        if (coords.x >= tileSize * 34){
            old_course = MovementDir::RIGHT;
            course = MovementDir::LEFT;
        }
    }
    else if (course == MovementDir::LEFT)
    {
        old_coords.x = coords.x;
        coords.x -= move_dist;
        if (coords.x <= tileSize * 14){
            old_course = MovementDir::LEFT;
            course = MovementDir::RIGHT;
        }
    }

    switch (old_course)
    {
    case MovementDir::RIGHT:

        DrawFloor(screen, Field, enemy_r);

        break;
    case MovementDir::LEFT:

        DrawFloor(screen, Field, enemy_l);

        break;
    default:
        break;
    }

    switch (course)
    {
    case MovementDir::LEFT:
        //DrawFloor(screen, Field, enemy_l);
        for (int y = coords.y; y < coords.y + tileSize * 2; ++y)
        {
            for (int x = coords.x; x < coords.x + tileSize * 2; ++x)
            {
                screen.PutPixel(x, y, mix(screen.GetPixel(x, y), enemy_l.GetPixel(x - coords.x, coords.y + tileSize * 2 - y - 1)));
            }
        }

        break;
    case MovementDir::RIGHT:
        //DrawFloor(screen, Field,enemy_r);
        for (int y = coords.y; y < coords.y + tileSize * 2; ++y)
        {
            for (int x = coords.x; x < coords.x + tileSize * 2; ++x)
            {
                screen.PutPixel(x, y, mix(screen.GetPixel(x, y), enemy_r.GetPixel(x - coords.x, coords.y + tileSize * 2 - y - 1)));
            }
        }
        break;
    default:
        break;
    }

    old_course = course;
}